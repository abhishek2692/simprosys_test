<?php


namespace App;


use Illuminate\Database\Eloquent\Model;


class UserPermission extends Model
{
    /**
     * The attributes that are mass assignable.
     *	
     * @var array
     */
    protected $table = 'role_has_permissions';

 	public function permission_details()
    {
        return $this->hasOne('App\Permission', 'id','permission_id');
    }   
}